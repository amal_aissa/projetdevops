package com.project.controller;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.project.bean.patients;
import com.project.repo.patientsRepo;

@RestController
public class theRestController {
	
	@Autowired
	patientsRepo repo;
	
	
//**********Show patients************
	@GetMapping(path="/patients")
	public List<patients> allPatients() {
		ArrayList <patients> list = new ArrayList<patients>();
		Iterator<patients> it = repo.findAll().iterator();
		while (it.hasNext())
			list.add(it.next());
		return list;
	}
	
	@GetMapping(path="/patient/{cin}")
	public patients getPatient(@PathVariable Integer cin) {
		
		return repo.findById(cin).get();
	}

	
//**********Create patient************
	@PostMapping(path="/addPatient")
	public void addPatient(@RequestBody patients patient) {
		
		repo.save(patient);
	}
	
//**********Update patient************
	@PutMapping(path="/updatePatient")
	public void updatePatient(@RequestBody patients patient) {
			
		repo.save(patient);
	}
	
//**********Delete patient************
	@DeleteMapping(path="/deletePatient")
	public void deletePatient(Integer cin) {
				
		repo.deleteById(cin);
	}
	
	
}
