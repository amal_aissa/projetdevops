package com.project.bean;


import javax.persistence.*;


@Entity
@Table(name="tbl_patients")
public class patients {
	
	@Id
	@Column(name="cin")
	private Integer cin;
	
	@Column(name="firstName")
	private String firstName;
	
	@Column(name="lastName")
	private String lastName;
	
	@Column(name="adress")
	private String adress;
	
	@Column(name="phone")
	private String phone;

//************constructors**************
	
	public patients() {
		super();
	}
	
	
	
	public patients(Integer cin, String firstName, String lastName, String adress, String phone) {
		super();
		this.cin = cin;
		this.firstName = firstName;
		this.lastName = lastName;
		this.adress = adress;
		this.phone = phone;
	}

//************getter & setter**************

	public Integer getCin() {
		return cin;
	}
	public void setCin(Integer cin) {
		this.cin = cin;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getAdress() {
		return adress;
	}
	public void setAdress(String adress) {
		this.adress = adress;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	
	
	
}
